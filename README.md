# hallasvizsgalat

## resources

### images
* dog.png
    * https://pixabay.com/vectors/corgi-dog-puppy-2026347/
* cat.png
    * https://pixabay.com/illustrations/cat-kawaii-feline-little-cat-5367741/
* horse.png
    * https://pixabay.com/vectors/horse-animals-cute-drawing-4210394/
* headphone icon (Tuan Nguyen)
    * https://www.iconfinder.com/icons/9139000/earphone_overears_headphone_music_audio_sound_icon

### sounds
* dog.mp3
    * https://freesound.org/people/apovedq/sounds/585495/
* cat.mp3
    * https://freesound.org/people/lextrack/sounds/333916/
* horse.mp3
    * https://freesound.org/people/UEPCOMANAM2011/sounds/394831/
